const fs = require("fs");

// ### 1
// function readFileCallback(err, data) {
//   if (err) {
//     console.error(err);
    
//   }
//   console.log(data);
// }
// fs.readFile("./file1.txt", "utf8", readFileCallback);
// fs.readFile("./file2.txt", "utf8", readFileCallback);

// ### 2 

// async function readFilePromise(path) {
//   try {
//     let content = await fs.promises.readFile(path, "utf-8");
//     const found = content.search("mutual");
//     if (found > -1) {
//       console.log("Chuỗi hợp lệ");
//     } else {
//       throw new Error("Chuỗi không hợp lệ");
//     }
//   } catch (err) {
//     console.log(err.message);
//   }
// }
// readFilePromise("./file1.txt");
// readFilePromise("./file2.txt");

// ###3


function readfileAync(dir) {
  var fileContent = fs.promises.readFile(dir, "utf-8");
  return fileContent;
}

function getAllFileContent(fileDirs) {
  var promises = [];
  for (const dir of fileDirs) {
    promises.push(readfileAync(dir));
  }
  return Promise.all(promises);
}

function getSum(stringArray) {
  let sum = 0;
  stringArray.forEach((e) => {
    let numberArray = e.match(/\d+/g).map(Number);
    console.log(numberArray);
    sum += numberArray.reduce((p, c) => p + c, 0);
  });

  return sum;
}

getAllFileContent(["./file1.txt", "./file2.txt"]).then((fileContents) => {
  console.log(getSum(fileContents));
});


// ### 4 
// (async (path1, path2) => {
//   try {
//     let count = 0;
//     let content1 = await fs.promises.readFile(path1, "utf-8");
//     let content2 = await fs.promises.readFile(path2, "utf-8");
//     count =
//       content1.match(/in/g).length +
//       content2.match(/in/g).length +
//       content1.match(/In/g).length +
//       content2.match(/In/g).length;
//     console.log(count);
//   } catch (error) {
//     console.log(error);
//   }
// })("./file1.txt", "./file2.txt");
